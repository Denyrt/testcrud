﻿namespace MyTestApp.Models
{
    public class Position
    {
        public uint Id { get; set; }
        public string Name { get; set; }
    }
}
