﻿using System.ComponentModel.DataAnnotations;

namespace MyTestApp.Models
{
    public class EmployeeRequestEdit
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public uint PositionId { get; set; }
    }
}
