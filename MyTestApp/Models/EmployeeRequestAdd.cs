﻿using System.ComponentModel.DataAnnotations;

namespace MyTestApp.Models
{
    public class EmployeeRequestAdd
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        public uint PositionId { get; set; }
    }
}
