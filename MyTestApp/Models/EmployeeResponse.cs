﻿namespace MyTestApp.Models
{    
    /// <summary>
    /// Модель для респонзов.
    /// </summary>
    public class EmployeeResponse
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Position { get; set; }        
    }
}
