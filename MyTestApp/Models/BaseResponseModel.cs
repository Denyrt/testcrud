﻿namespace MyTestApp.Models
{
    public class BaseResponseModel<T>
    {
        public bool State { get; set; }
        
        public string Error { get; set; }

        public T Data { get; set; }

        public static BaseResponseModel<T> SuccessResponse(T data) => new BaseResponseModel<T>
        {
            State = true,
            Error = string.Empty,
            Data = data
        };

        public static BaseResponseModel<T> ErrorResponse(string error, T data) => new BaseResponseModel<T>
        {
            State = false,
            Error = error,
            Data = data
        };

        public static BaseResponseModel<object> ErrorResponse(string error) => new BaseResponseModel<object>
        {
            State = false,
            Error = error,
            Data = null
        };
    }
}
