﻿using MyTestApp.Repositories;
using MyTestApp.Models;
using System.Collections.Generic;
using System.Linq;
using MyTestApp.Data;
using System;
using MyTestApp.Exceptions;
using System.Threading.Tasks;

namespace MyTestApp.Services
{
    public class EmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPositionRepository _positionRepository;

        public EmployeeService(IEmployeeRepository employeeRepository, IPositionRepository positionRepository)
        {
            _employeeRepository = employeeRepository;
            _positionRepository = positionRepository;
        }

        public async Task<EmployeeResponse> GetById(object id)
        {
            var employee = await _employeeRepository.GetById(id);

            return employee == null ? null : new EmployeeResponse
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Position = (await _positionRepository.GetById(employee.PositionId))?.Name
            };
        }

        public async Task<IEnumerable<EmployeeResponse>> GetAll(PageFilter pageFilter)
        {
            if (!pageFilter.Validate()) pageFilter = new PageFilter();

            return (await _employeeRepository.Get(pageFilter, includeProperties: "Position"))
                .Select(employee => new EmployeeResponse 
                {
                    Id = employee.Id,
                    FullName = employee.FullName,
                    Position = employee.Position.Name
                });
        }

        public async Task<EmployeeResponse> Create(EmployeeRequestAdd employee)
        {
            try
            {
                var position = await _positionRepository.GetById(employee.PositionId);                
                if (position == null) throw new NotExistsException("Position Id Not Exists");


                var insert = await _employeeRepository.Insert(new Employee
                {
                    FullName = employee.FullName,
                    PositionId = employee.PositionId
                });

                return new EmployeeResponse
                {
                    Id = insert.Id,
                    FullName = insert.FullName,
                    Position = position.Name
                };
            }
            catch(UniqueException ex)
            {
                throw new UniqueException("Запись с таким ФИО уже существует", ex);
            }            
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var entry = await _employeeRepository.GetById(id);

                if (entry == null) throw new NotExistsException("Employee Id not exists.");

                await _employeeRepository.Delete(id);
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }                       
        }        

        public async Task<EmployeeResponse> Update(EmployeeRequestEdit employee)
        {
            try
            {
                var entity = await _employeeRepository.GetById(employee.Id);
                if (entity == null) throw new NotExistsException("Employee Id not exists.");

                var position = await _positionRepository.GetById(employee.PositionId);
                if (position == null) throw new NotExistsException("Position Id not exists.");

                entity.FullName = employee.FullName;
                entity.PositionId = employee.PositionId;                
                var updated = await _employeeRepository.Update(entity);                                

                return new EmployeeResponse
                {
                    Id = updated.Id,
                    FullName = updated.FullName,
                    Position = position.Name
                };
            }            
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
