﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyTestApp.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetById(object id);

        Task<IEnumerable<TEntity>> Get(PageFilter pageFilter = null, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, 
            string includeProperties = "");

        Task Delete(TEntity entityToDelete);

        Task Delete(object id);                        

        Task<TEntity> Insert(TEntity entity);

        Task<TEntity> Update(TEntity entityToUpdate);
    }
}
