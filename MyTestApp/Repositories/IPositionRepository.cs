﻿using MyTestApp.Data;

namespace MyTestApp.Repositories
{
    public interface IPositionRepository : IRepository<Position>
    {

    }
}
