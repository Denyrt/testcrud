﻿namespace MyTestApp.Repositories
{
    /// <summary>
    /// Фильтр для пагинации. Нумерация страниц с начинается 0.
    /// </summary>
    public class PageFilter
    {
        public int NumberOfPage { get; set; } = 100;

        public int CountInPage { get; set; } = 0;

        public int SkipValue() => NumberOfPage * CountInPage;

        public int TakeValue() => CountInPage;

        public bool Validate() => NumberOfPage >= 0 && CountInPage > 0;        
    }
}
