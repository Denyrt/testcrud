﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MyTestApp.Data;
using MyTestApp.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyTestApp.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        internal ApplicationDbContext context;

        public EmployeeRepository(ApplicationDbContext dbContext)
        {
            context = dbContext;
        }

        public async Task Delete(object id)
        {
            var entityToDelete = await context.Employees.FindAsync(id);
            await Delete(entityToDelete);
        }

        public async Task Delete(Employee entityToDelete)
        {
            context.Employees.Remove(entityToDelete);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Employee>> Get(PageFilter pageFilter = null, Expression<Func<Employee, bool>> filter = null, Func<IQueryable<Employee>, IOrderedQueryable<Employee>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<Employee> query = context.Employees;
            
            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (pageFilter?.Validate() == true)
            {
                query = query.Skip(pageFilter.SkipValue()).Take(pageFilter.TakeValue());
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public async Task<Employee> GetById(object id)
        {
            return await context.Employees.FindAsync(id);
        }

        public async Task<Employee> Insert(Employee entity)
        {
            try
            {
                var insert = await context.Employees.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                if (ex.InnerException is SqlException sqlEx && sqlEx.Number == 2601)
                {
                    throw new UniqueException("Запись существует.");
                }

                throw;
            }

            return await GetById(entity.Id);
        }

        public async Task<Employee> Update(Employee entityToUpdate)
        {
            try
            {
                var update = context.Employees.Update(entityToUpdate);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (ex.InnerException is SqlException sqlEx && sqlEx.Number == 2601)
                {
                    throw new UniqueException("Запись существует.");
                }

                throw;
            }

            return await GetById(entityToUpdate.Id);
        }
    }
}
