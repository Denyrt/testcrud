﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MyTestApp.Data;
using MyTestApp.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyTestApp.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        internal ApplicationDbContext context;        

        public PositionRepository(ApplicationDbContext dbContext)
        {
            context = dbContext; 
        }

        public async Task Delete(object id)
        {
            var entityToDelete = await context.Positions.FindAsync(id);
            await Delete(entityToDelete);
        }

        public async Task Delete(Position entityToDelete)
        {
            context.Positions.Remove(entityToDelete);
            await context.SaveChangesAsync();
        }        

        public async Task<IEnumerable<Position>> Get(PageFilter pageFilter = null, Expression<Func<Position, bool>> filter = null,
            Func<IQueryable<Position>, IOrderedQueryable<Position>> orderBy = null, string includeProperties = "")
        {
            IQueryable<Position> query = context.Positions;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (pageFilter?.Validate() == true)
            {
                query = query.Skip(pageFilter.SkipValue()).Take(pageFilter.SkipValue());
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public async Task<Position> GetById(object id)
        {
            return await context.Positions.FindAsync(id);
        }

        public async Task<Position> Insert(Position entity)
        {
            try
            {
                await context.Positions.AddAsync(entity);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (ex.InnerException is SqlException sqlEx && sqlEx.Number == 2601)
                {
                    throw new UniqueException("Запись существует.");
                }

                throw;
            }

            return entity;
        }

        public async Task<Position> Update(Position entityToUpdate)
        {
            try
            {
                context.Positions.Update(entityToUpdate);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                if (ex.InnerException is SqlException sqlEx && sqlEx.Number == 2601)
                {
                    throw new UniqueException("Запись существует.");
                }

                throw;
            }

            return await GetById(entityToUpdate.Id);
        }
    }
}
