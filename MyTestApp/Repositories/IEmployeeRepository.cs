﻿using MyTestApp.Data;

namespace MyTestApp.Repositories
{
    public interface IEmployeeRepository : IRepository<Employee>
    {

    }
}
