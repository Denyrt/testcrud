﻿using Microsoft.AspNetCore.Mvc;
using MyTestApp.Exceptions;
using MyTestApp.Models;
using MyTestApp.Repositories;
using MyTestApp.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyTestApp.Controllers
{
    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService _employeeService;

        public EmployeeController(EmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var employee = await _employeeService.GetById(id);
            var response = BaseResponseModel<EmployeeResponse>.SuccessResponse(employee);
            return Ok(response);
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetAll([FromQuery]PageFilter pageFilter)
        {
            var employeies = await _employeeService.GetAll(pageFilter);            
            var response = BaseResponseModel<IEnumerable<EmployeeResponse>>.SuccessResponse(employeies);
            return Ok(response);
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var removed = await _employeeService.Delete(id);
                var response = BaseResponseModel<object>.SuccessResponse(new { removed });
                return Ok(response);
            }
            catch (NotExistsException ex)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse(ex.Message));
            }
            catch (Exception)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse("Server Error."));
            }
        }        

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody] EmployeeRequestEdit employee)
        {
            try
            {
                var updated = await _employeeService.Update(employee);
                return Ok(BaseResponseModel<EmployeeResponse>.SuccessResponse(updated));
            }
            catch(UniqueException ex)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse(ex.Message));
            }
            catch(NotExistsException ex)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse(ex.Message));
            }
            catch (Exception)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse("Server Error."));
            }
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody] EmployeeRequestAdd employee)
        {
            try
            {
                var added = await _employeeService.Create(employee);
                return Ok(BaseResponseModel<EmployeeResponse>.SuccessResponse(added));
            }
            catch(UniqueException ex)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse(ex.Message));
            }
            catch (NotExistsException ex)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse(ex.Message));
            }
            catch (Exception)
            {
                return BadRequest(BaseResponseModel<object>.ErrorResponse("Server Error."));
            }
        } 
    }
}
