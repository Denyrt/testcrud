﻿namespace MyTestApp.Data
{
    public class Position
    {
        public uint Id { get; set; }

        public string Name { get; set; }
    }
}
