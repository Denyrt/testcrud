﻿using Microsoft.EntityFrameworkCore;

namespace MyTestApp.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Position> Positions { get; set; }

        public ApplicationDbContext()
        {
            Database.EnsureCreated();
        }        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=mydb;User Id=denis;password=denis;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
            .HasIndex(employee => employee.FullName)
            .IsUnique(true);

            base.OnModelCreating(modelBuilder);
        }
    }
}
