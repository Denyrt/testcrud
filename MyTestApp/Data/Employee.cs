﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyTestApp.Data
{
    public class Employee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int Id { get; set; }

        [Required, StringLength(65)]        
        public string FullName { get; set; }

        public uint PositionId { get; set; }

        public virtual Position Position { get; set; }
    }
}
