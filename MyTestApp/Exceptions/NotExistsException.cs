﻿using System;

namespace MyTestApp.Exceptions
{
    public class NotExistsException : Exception
    {
        public NotExistsException() : base()
        {

        }

        public NotExistsException(string message) : base(message)
        {

        }

        public NotExistsException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
